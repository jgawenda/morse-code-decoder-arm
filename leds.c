#include "MKL46Z4.h"                    
#include "leds.h"												
#include "string.h"

const uint32_t red_mask= 1UL<<5;				
const uint32_t green_mask= 1UL<<29;			
			
void ledsInitialize(void) {
volatile int delay;
	

  SIM->SCGC5 |=  (SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK);      
  PORTD->PCR[5] = PORT_PCR_MUX(1);                       
  PORTE->PCR[29] = PORT_PCR_MUX(1);                     
  
	FPTD->PSOR = red_mask	;          
	FPTE->PSOR = green_mask	;        
  FPTD->PDDR = red_mask	;          
	FPTE->PDDR = green_mask	;       	
}

void ledRedOn (void) {
	FPTD->PCOR=red_mask;          	
	FPTE->PSOR=green_mask;         
}

void ledGreenOn (void) {
	FPTE->PCOR=green_mask;       		
	FPTD->PSOR=red_mask;          	
}

void ledsOff (void) {
		FPTD->PSOR=red_mask;          
	  FPTE->PSOR=green_mask;        
}

void ledsOn (void) {
		FPTD->PCOR=red_mask;      	
	  FPTE->PCOR=green_mask;     	
}

void ledToggle(void){
	FPTE->PTOR=green_mask;       		/* switch Green LED on */
	FPTD->PTOR=red_mask;          	/* switch Red LED off  */
}



