#ifndef pit_h
#define pit_h

#include "MKL46Z4.h" 

void initialize_pit(void);
void reset_pit(void);
void disable_short_pit(void);
void enable_short_pit(void);
void disable_long_pit(void);
void enable_long_pit(void);

#endif