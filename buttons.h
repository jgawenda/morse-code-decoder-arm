#ifndef buttons_h
#define buttons_h

#include "MKL46Z4.h"                    // Device header

void buttonsInitialize(void);
int32_t buttonRead(void);

#endif

