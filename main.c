#include "MKL46Z4.h"                    	
#include "leds.h"													
#include "buttons.h"	
#include "slcd.h"
#include "pit.h"
#include "string.h"
#include <stdbool.h>
#include "extra.h"

//TODO: UART DO WYSYLANIA LITER

#define SW1_PIN 3												
#define SW3_PIN 12

#define PORTC_D_IRQ_NBR (PORTC_PORTD_IRQn)

char morse_code[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int morse_index = 0;

char word[200] = "   ";
int word_length = 3;

int word_index = 0;

void clear_morse(void);
void decode_morse(char * morse_code);

int main (void) {
	
	// initialization
	slcdInitialize();
	ledsInitialize();  
	buttonsInitialize();
	initialize_pit();
	
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 3);
	// main loop
	while(1){	
	}
}
// used for displaying decoded letters on the 7-seg LCD screen
void SysTick_Handler(void) 
	{
		slcdDisplayLetter(word, word_length, word_index);
		// check if index pointing at the letter is larger than the word's length
		if (word_index <= word_length) {
			// increment index
			word_index++;
		}
		else {
			// set index to 0 (loop back to beginning)
			word_index = 0;
	}
}

// interrupt from PIT timer
void PIT_IRQHandler(void){
	
	// interrupt from shorter timer - decodes given morse code after a period of time
	if (PIT-> CHANNEL[0].TFLG & PIT_TFLG_TIF_MASK){
	// decodes morse code sequence and appends the result to "word[]"
	decode_morse(morse_code);
	// clears array 
	clear_morse();
	// disables PIT timer
	disable_short_pit();
	
	// clears interrupt flag
	PIT -> CHANNEL[0].TFLG |= PIT_TFLG_TIF_MASK;
	}
	
	// appends an empty space to "word[]" after a period of time
	else if (PIT -> CHANNEL[1].TFLG & PIT_TFLG_TIF_MASK){
		// appends an empty space and increments word_length by 1
		strncat(word, " ", 1);
		word_length++;
		
		// disables timer
		disable_long_pit();
		
		// clears interrupt flag
		PIT -> CHANNEL[1].TFLG |= PIT_TFLG_TIF_MASK;
	}
	
}

// interrupt from right button
void PORTC_PORTD_IRQHandler(void){ 	
	
	int i = 0;
	bool long_pulse = false;

	FPTD->PSOR = 1UL << 5;
	FPTE->PSOR = 1UL << 29;
	
	// disables PIT timer
	disable_short_pit();
	disable_long_pit();

	// measures how long the button is pressed
	while((FPTC->PDIR&(1<<SW1_PIN))==0) {
		i++;
		if (i > 1000000) long_pulse = true;
	}	
	
	// if it's a '-'
	if (long_pulse == true) {
		FPTD->PCOR = 1UL << 5;
		morse_code[morse_index] = '-';
		morse_index++;
	}
	
	// if it's a '.'
	else {
		FPTE->PCOR = 1UL << 29;
		morse_code[morse_index] = '.';
		morse_index++;
	}
	
	// enables PIT timer to wait for a period of time
	enable_short_pit();
	enable_long_pit();
		
	// clears interrupt flag
	PORTC->PCR[SW1_PIN] |= PORT_PCR_ISF_MASK; 
}


//decoding morse code
// argument is array of 5 chars with '.' and '-'
void decode_morse(char * morse_code) {
	if (!(strcmp(morse_code, ".-"))) {
		strncat(word, "a", 1);
	}
	else if (!(strcmp(morse_code, "-..."))) {
		strncat(word, "b" ,1);
	}
	else if (!(strcmp(morse_code, "-..."))) {
		strncat(word, "b" ,1);
	}
	else if (!(strcmp(morse_code, "-.-."))) {
		strncat(word, "c" ,1);
	}
	else if (!(strcmp(morse_code, "-.."))) {
		strncat(word, "d" ,1);
	}
	else if (!(strcmp(morse_code, "."))) {
		strncat(word, "e" ,1);
	}
	else if (!(strcmp(morse_code, "..-."))) {
		strncat(word, "f" ,1);
	}
	else if (!(strcmp(morse_code, "--."))) {
		strncat(word, "g" ,1);
	}
	else if (!(strcmp(morse_code, "...."))) {
		strncat(word, "h" ,1);
	}
	else if (!(strcmp(morse_code, ".."))) {
		strncat(word, "i" ,1);
	}
	else if (!(strcmp(morse_code, ".---"))) {
		strncat(word, "j" ,1);
	}
	else if (!(strcmp(morse_code, "-.-"))) {
		strncat(word, "k" ,1);
	}
	else if (!(strcmp(morse_code, ".-.."))) {
		strncat(word, "l" ,1);
	}
	else if (!(strcmp(morse_code, "--"))) {
		strncat(word, "m" ,1);
	}
	else if (!(strcmp(morse_code, "-."))) {
		strncat(word, "n" ,1);
	}
	else if (!(strcmp(morse_code, "---"))) {
		strncat(word, "o" ,1);
	}
	else if (!(strcmp(morse_code, ".--."))) {
		strncat(word, "p" ,1);
	}
	else if (!(strcmp(morse_code, "--.-"))) {
		strncat(word, "q" ,1);
	}
	else if (!(strcmp(morse_code, ".-."))) {
		strncat(word, "r" ,1);
	}
	else if (!(strcmp(morse_code, "..."))) {
		strncat(word, "s" ,1);
	}
	else if (!(strcmp(morse_code, "-"))) {
		strncat(word, "t" ,1);
	}
	else if (!(strcmp(morse_code, "..-"))) {
		strncat(word, "u" ,1);
	}
	else if (!(strcmp(morse_code, "...-"))) {
		strncat(word, "v" ,1);
	}
	else if (!(strcmp(morse_code, ".--"))) {
		strncat(word, "w" ,1);
	}
	else if (!(strcmp(morse_code, "-..-"))) {
		strncat(word, "x" ,1);
	}
	else if (!(strcmp(morse_code, "-.--"))) {
		strncat(word, "y" ,1);
	}
	else if (!(strcmp(morse_code, "--.."))) {
		strncat(word, "z" ,1);
	}
	else {
		strncat(word, "-", 1);
	}
	
	word_length++;
}

// clearing morse code array
void clear_morse(){
	int i = 0;
	for (i = 0; i < 10; i++){
		morse_code[i] = '\0';
	}
	morse_index = 0;
}