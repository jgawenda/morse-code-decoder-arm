/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    lptimer.h
 *      Purpose: Microprocessors Laboratory
 *----------------------------------------------------------------------------
 *      
 *      Author: Pawel Russek AGH University of Science and Technology
 *---------------------------------------------------------------------------*/

#ifndef LPTIMER_H
#define LPTIMER_H

void lpTimerInitialize(void);
void fastSlowSwitch(void);

#endif
